# DigitalDistributionBackupServer

Currently supports; 

* **Steam**
* **uPlay**
* **GOG (Other Downloads) / Backup Installers**

Planned;

* *GOG Galaxy Installed Games*
* *Origin*
* *Battle.NET*
* *Microsoft Store*
* *Others?*




**/Scripts** - *Bash Scripts to automate backing up of Steam/uPlay and other Digital Distribution services.*


**/www** - *Webserver files for monitoring Scripts*
