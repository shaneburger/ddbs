#!/bin/bash

#Edit this to point to your uPlay mounted Directory, in Docker version this will be hardcoded to /data/uPlay/In
#INCLUDE ENDING /
uPlayGameDirectory="/mnt/Games/uPlay/"

#Edit this to point to the place where you want your backup's stored, in Docker version this will be hardcoded to /data/uPlay/Out
#INCLUDE ENDING /
uPlayBackupDirectory="/mnt/Server/Backups/uPlay/"

#Edit this to an user account that you want the resulting archives to use
validUser="username"
#Edit this to an account group that you want the resulting archives to use
validGroup="group"

# Do not edit below
wwwPath=/srv/http/
lockFile=/tmp/GameBackup.running

display_title(){
	dtdt=$(date '+%d/%m/%Y - %H:%M:%S');
	echo " [ $dtdt ] uPlay Game Backup - Started!"
}
display_end(){
	dedt=$(date '+%d/%m/%Y - %H:%M:%S');
	echo " [ $dedt ] uPlay Game Backup - Finished!"
}
create_log(){
touch $wwwPath/log.html
}
raise_error() {
	local error_message="$@"
	echo "${error_message}" 1>&2;
}
set_running(){
cp $wwwPath/isrunning.html $wwwPath/running.html
chmod 777 $wwwPath/running.html
chown http:http $wwwPath/running.html
}
set_notrunning(){
cp $wwwPath/isnotrunning.html $wwwPath/running.html
chmod 777 $wwwPath/running.html
chown http:http $wwwPath/running.html
}
start_program(){	
	NewGameName=${BadGameName//[\''"?*%#^!@$&/()=+[]{};.,`~']}	
	NextTrimm="$(echo -e "${NewGameName}" | sed -e 's/^[[:space:]]*//')"
	Trimmed="$(echo -e "${NextTrimm}" | sed -e 's/[[:space:]]*$//')"	
	GameName=${Trimmed//['<>:\|']/''}
	ZipFileName="$uPlayBackupDir$GameName.zip"	
	if [ -e "$ZipFileName" ]	
		then
			echo "DONE: $GameName REASON: Already archived"
		else
			if [ -z "$(ls -A "$uPlayGameDir$BadGameName/uplay_download")" ]; then
				echo "INFO: Begin backing up, $GameName"
				dt=$(date '+%d/%m/%Y - %H:%M:%S');
				create_log
				echo "[ <font color="yellow">$dt</font> ] <font color="red">Commence Backup</font> :: <font color="DeepSkyBlue">uPlay</font> / <b>$GameName</b> <br/>" >> $wwwPath/log.html
				chown http:http $wwwPath/log.html
				chmod 777 $wwwPath/log.html
				zip -1 -r -q "$ZipFileName" "$BadGameName/"
				if [ -e "$ZipFileName" ]
				then
					chown "$validUser":"$validGroup" "$ZipFileName"
					chmod 777 "$ZipFileName"
					echo "INFO: Finished backing up, $GameName"
					dt=$(date '+%d/%m/%Y - %H:%M:%S');
					create_log
					echo "[ <font color="yellow">$dt</font> ] <font color="green">Backup Complete</font> :: <font color="DeepSkyBlue">uPlay</font> / <b>$GameName</b> <br/><br/>" >> $wwwPath/log.html
					chown http:http $wwwPath/log.html
					chmod 777 $wwwPath/log.html
				else
					echo "ERROR: Backing up FAILED! $GameName"
					dt=$(date '+%d/%m/%Y - %H:%M:%S');
					create_log
					echo "[ <font color="yellow">$dt</font> ] <font color="red">Backup Failed!!</font> :: <font color="DeepSkyBlue">uPlay</font> / <b>$GameName</b> <br/><br/>" >> $wwwPath/log.html
					chown http:http $wwwPath/log.html
					chmod 777 $wwwPath/log.html
				fi
			else				
				echo "SKIP: $GameName REASON: Currently Downloading..."
			fi
	fi	
}
display_title
cd "$uPlayGameDirectory/games"
if [ ! -e $lockFile ]
	then
		touch $lockFile
		set_running
		for D in *; do
			if [ -d "${D}" ]; then
				BadGameName="${D}"
				uPlayGameDir="$uPlayGameDirectory/games/"
				uPlayBackupDir="$uPlayBackupDirectory"
				start_program
			fi
		done
		rm $lockFile
		set_notrunning
	else
		raise_error "ERROR: Script already Running!"
fi
display_end