#!/bin/bash

#Edit this to point to your Steam mounted Directory, in Docker version this will be hardcoded to /data/Steam/In
#INCLUDE ENDING /
SteamGameDirectory="/mnt/Games/Steam/"

#Edit this to point to the place where you want your backup's stored, in Docker version this will be hardcoded to /data/Steam/Out
#NO ENDING /
SteamBackupDirectory="/mnt/Server/Backups/Steam"

#Edit this to an user account that you want the resulting archives to use
validUser="username"
#Edit this to an account group that you want the resulting archives to use
validGroup="group"

# Do not edit below
wwwPath=/srv/http/
lockFile=/tmp/GameBackup.running

display_title(){
	dtdt=$(date '+%d/%m/%Y - %H:%M:%S');
	echo " [ $dtdt ] Steam Game Backup - Started!"
}
display_end(){
	dedt=$(date '+%d/%m/%Y - %H:%M:%S');
	echo " [ $dedt ] Steam Game Backup - Finished!"
}
create_log(){
touch $wwwPath/log.html
}
raise_error() {
	local error_message="$@"
	echo "${error_message}" 1>&2;
}
set_running(){
cp $wwwPath/isrunning.html $wwwPath/running.html
chmod 777 $wwwPath/running.html
chown http:http $wwwPath/running.html
}
set_notrunning(){
cp $wwwPath/isnotrunning.html $wwwPath/running.html
chmod 777 $wwwPath/running.html
chown http:http $wwwPath/running.html
}
start_program() {

	selectedLine=$(cat $steamACFFile | grep "installdir")
	installDirEnd="$(cut -d'"' -f4 <<<$selectedLine)"
	selectedLine=$(cat $steamACFFile | grep "\"name\"")
	BadGameName="$(cut -d'"' -f4 <<<$selectedLine)"
	NewGameName=${BadGameName//[\''"?*%#^!@$&/()=+[]{};.,`~']}	
	NextTrimm="$(echo -e "${NewGameName}" | sed -e 's/^[[:space:]]*//')"
	Trimmed="$(echo -e "${NextTrimm}" | sed -e 's/[[:space:]]*$//')"	
	GameName=${Trimmed//['<>:\|']/''}
	oldZipName="$steamBackupDir$installDirEnd.zip"
	ZipFileName="$steamBackupDir$GameName/$installDirEnd.zip"

	if [ ! -d "$steamBackupDir$GameName" ]
		then
			mkdir "$steamBackupDir$GameName"
	fi	
	
	commonAdd="common/"
	steamGameDir="$steamACFDir$commonAdd$installDirEnd"
	steamGameShortDir="$commonAdd$installDirEnd"
	
	cd $steamACFDir
	
	if [ -e "$ZipFileName" ]
		then
			echo "DONE: $GameName REASON: Already archived"
		else
			if [ -z "$(ls -A "$steamGameDir")" ]; then
				echo "SKIP: $GameName REASON: Currently Downloading..."
			else
				echo "INFO: Begin backing up, $GameName"
				dt=$(date '+%d/%m/%Y - %H:%M:%S');
				create_log
				echo "[ <font color="yellow">$dt</font> ] <font color="red">Commence Backup</font> :: <font color="OliveDrab">STEAM</font> / <b>$GameName</b> <br/>" >> $wwwPath/log.html
				chown http:http $wwwPath/log.html
				chmod 777 $wwwPath/log.html
				zip -1 -r -q "$ZipFileName" "$steamACFFileShort" "$steamGameShortDir/"				
				if [ -e "$ZipFileName" ]
				then
					chown "$validUser":"$validGroup" -R "$steamBackupDir$GameName/" 
					chmod 777 -R "$steamBackupDir$GameName/"
					echo "INFO: Finished backing up, $GameName"
					dt=$(date '+%d/%m/%Y - %H:%M:%S');
					create_log
					echo "[ <font color="yellow">$dt</font> ] <font color="green">Backup Complete</font> :: <font color="OliveDrab">STEAM</font> / <b>$GameName</b> <br/><br />" >> $wwwPath/log.html
					chown http:http $wwwPath/log.html
					chmod 777 $wwwPath/log.html
				else
					chown "$validUser":"$validGroup" -R "$steamBackupDir$GameName/" 
					chmod 777 -R "$steamBackupDir$GameName/"
					echo "ERROR: Backing up FAILED! $GameName"
					dt=$(date '+%d/%m/%Y - %H:%M:%S');
					create_log
					echo "[ <font color="yellow">$dt</font> ] <font color="red">Backup Failed!!</font> :: <font color="OliveDrab">STEAM</font> / <b>$GameName</b> <br/><br />" >> $wwwPath/log.html
					chown http:http $wwwPath/log.html
					chmod 777 $wwwPath/log.html			
				fi
			fi
	fi
}
start_program_check(){	
	if [ -e $steamACFFile ]
		then
			start_program
	fi		
}
display_title
cd $SteamGameDirectory/steamapps
if [ ! -e "$lockFile" ]
	then
		touch "$lockFile"	
		set_running
		for steamACF in ./*.acf; do		
			prefix="./appmanifest_"
			suffix=".acf"
			steamID=${steamACF#"$prefix"}
			steamID=${steamID%"$suffix"}	
			steamACFDir="$SteamGameDirectory/steamapps/"
			steamACFName="appmanifest_$steamID"
			steamACFFile="$steamACFDir$steamACFName$steam.acf"
			steamBackupDir="$SteamBackupDirectory/"
			steamACFFileShort="./$steamACFName$steam.acf"
			start_program_check
		done
		rm "$lockFile"
		set_notrunning
	else
		raise_error "ERROR: Script already Running!"
fi
display_end