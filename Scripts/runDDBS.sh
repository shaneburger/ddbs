#!/bin/bash

#Edit this to Script location
scriptLoc="/etc/cron.hourly/"

#Do not edit below

echo "Begin DDBS"
cd "$scriptLoc"
./SteamBackup.sh
cd "$scriptLoc"
./uPlayBackup.sh
cd "$scriptLoc"
./GOGOtherDownloadsBackup.sh
cd "$scriptLoc"
echo "End DDBS"
